import "../CSS/header.css";

const Header = () => {
  return (
    <div className="company">
      <h1>Money Exchanger</h1>
      <img alt="money img" src="money-exchange.png"></img>
    </div>
  );
};

export default Header;
