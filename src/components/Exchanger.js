import "../CSS/exchanger.css";
import Button from "./Button";
import Select from "./Select";
import Input from "./Input";
import { useState, useEffect } from "react";

const Exchanger = () => {
  const [loading, setLoading] = useState(true);
  const [posts, setPosts] = useState([]);
  const [text, setText] = useState();
  const [value, setValue] = useState();
  const [inputValue, setInputValue] = useState();

  useEffect(() => {
    try {
      fetch("https://api.nbp.pl/api/exchangerates/tables/a/")
        .then((response) => response.json())
        .then((data) => {
          setPosts(data[0].rates);
          setLoading(false);
        });
    } catch (error) {
      setLoading(false);
    }
  }, []);

  const preventMinus = (e) => {
    if (e.code === "Minus") {
      e.preventDefault();
    }
  };

  const input = (event) => {
    setInputValue(event.target.value);
  };

  const selectedItem = (event) => {
    const index = event.nativeEvent.target.selectedIndex;
    setText(event.nativeEvent.target[index].text);
    setValue(event.nativeEvent.target[index].value);
  };

  return loading ? (
    <p>Loading...</p>
  ) : (
    <div className="exchanger">
      <Input input={input} preventMinus={preventMinus} />
      <Select selectedItem={selectedItem} posts={posts} />
      <Button value={value} text={text} inputValue={inputValue} />
    </div>
  );
};

export default Exchanger;
