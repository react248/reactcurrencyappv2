const Select = ({ selectedItem, posts }) => {
  return (
    <select onChange={(event) => selectedItem(event)}>
      <option>Choose the currency</option>
      {posts.map((post, index) => {
        if (post.code === "USD" || post.code === "CHF" || post.code === "EUR")
          return (
            <option value={post.mid} key={index}>
              {post.code}
            </option>
          );
      })}
    </select>
  );
};

export default Select;
