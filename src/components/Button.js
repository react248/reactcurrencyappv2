import { useState } from "react";
import "../CSS/button.css";

const Button = ({ value, text, inputValue }) => {
  const [paragraphText, setParagraphText] = useState("");
  const exchange = () => {
    if (isNaN(value)) {
      setParagraphText("You need to choose USD, EUR OR CHF");
    } else {
      setParagraphText(
        `Exchanging ${inputValue} ${text} at the rate of ${value} PLN for 1.00 ${text} will give you ${(
          inputValue * value
        ).toFixed(2)} PLN`
      );
    }
  };

  return (
    <div>
      <button onClick={exchange}>Exchange</button>
      <p>{paragraphText}</p>
    </div>
  );
};

export default Button;
